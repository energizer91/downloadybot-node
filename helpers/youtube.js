/**
 * Created by Александр on 21.01.2017.
 */

module.exports = function (configs) {
    var config = configs.youtube,
        youtube = require('youtube-dl');

    return {
        getVideo: function (link, params) {
            var video = youtube(link, params, {cwd: __dirname});

            video.on('info', function(info) {
                console.log('Download started');
                console.log('filename: ' + info.filename);
                console.log('size: ' + info.size);
            });

            return video;
        }
    };
};