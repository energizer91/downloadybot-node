/**
 * Created by Александр on 27.12.2015.
 */

module.exports = function (configs) {
    var config = configs.vk,
        requestHelper = require('./request')(configs);

    return {
        executeCommand: function(command, params, method) {
            var vkUrl = config.url + command,
                parameters = requestHelper.prepareConfig(vkUrl, method);

            if (config.api_version) {
                params.v = config.api_version;
            }

            return requestHelper.makeRequest(parameters, params);
        },
        getPostById: function (postId, params) {
            if (!params) {
                params = {};
            }

            params.posts = config.group_id + '_' + postId;
            console.log('Making VK request wall.getById', params);
            return this.executeCommand('wall.getById', params, 'GET');
        },
        getVideoById: function (videoId, params) {
            if (!params) {
                params = {};
            }

            params.video = config.group_id + '_' + videoId;
            console.log('Making VK request video.getById', params);
            return this.executeCommand('video.getById', params, 'GET');
        }
    };
};