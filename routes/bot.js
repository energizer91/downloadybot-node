/**
 * Created by Александр on 13.12.2015.
 */
module.exports = function (express, botApi, configs) {
    var router = express.Router(),
        q = require('q');

    var commands = {
            '/start': function (command, message, user) {
                return botApi.bot.sendMessage(message.chat.id, 'start');
            },
            '/help': function (command, message, user) {
                return botApi.bot.sendMessage(message.chat.id, 'start');
            }
        },
        performCommand = function (command, data, user) {
            return commands[command[0]].call(botApi.bot, command, data, user);
        }/*,
        performInline = function (query, params) {
            var results = [],
                aneks_count = 5,
                searchAction;
            if (!params) {
                params = {};
            }
            if (!query.query) {
                searchAction = botApi.mongo.Anek.find({text: {$ne: ''}})
                    .sort({date: -1})
                    .skip(query.offset || 0)
                    .limit(aneks_count)
                    .exec();
            } else {
                searchAction = searchAneks(query.query, aneks_count, query.offset || 0);
            }
            return searchAction.then(function (aneks) {
                results = aneks.map(function (anek) {
                    return {
                        type: 'article',
                        id: anek.post_id.toString(),
                        title: botApi.dict.translate(params.language, 'anek_number', {number: anek.post_id}),
                        input_message_content: {
                            message_text: anek.text,
                            parse_mode: 'HTML'
                        },
                        //message_text: anek.text,
                        description: anek.text.slice(0, 100)
                    };
                });

                return botApi.bot.sendInline(query.id, results, query.offset + aneks_count);
            }).catch(function () {
                return botApi.bot.sendInline(query.id, results, query.offset + aneks_count);
            });
        }*/,
        performCallbackQuery = function (queryData, data, params) {
            if (!params) {
                params = {};
            }
            /*switch (queryData[0]) {
                case 'comment':
                    var aneks = [];
                    return getAllComments(queryData[1]).then(function (comments) {
                        comments.forEach(function (comment) {
                            aneks = aneks.concat(comment.response.items);
                        });
                        aneks = aneks.sort(function (a, b) {
                            return b.likes.count - a.likes.count;
                        }).slice(0, 3).map(function (comment, index) {
                            comment.text = botApi.dict.translate(params.language, 'th_place', {nth: (index + 1)}) + comment.text;
                            comment.reply_to_message_id = data.callback_query.message.message_id;
                            return comment;
                        });

                        params.disableButtons = true;
                        params.forceAttachments = true;

                        return botApi.bot.answerCallbackQuery(data.callback_query.id)
                            .finally(function () {
                                return botApi.bot.sendMessages(data.callback_query.message.chat.id, aneks, params);
                                // .finally(function () {
                                //     var editedMessage = {
                                //         chat_id: data.callback_query.message.chat.id,
                                //         message_id: data.callback_query.message.message_id,
                                //         disableComments: true
                                //     };
                                //
                                //     return botApi.bot.editMessageButtons(editedMessage);
                                // })
                                // message editing is temporary disabled
                            });
                    });
                    break;
                case 'attach':
                    return botApi.vk.getPostById(queryData[1]).then(function (posts) {
                        var post = posts.response[0];

                        if (!post) {
                            throw new Error('Post not found');
                        }

                        if (!post.attachments && !post.copy_history) {
                            throw new Error('Attachments not found');
                        }

                        while (!post.attachments && post.copy_history) {
                            post = post.copy_history[0];
                        }

                        return botApi.bot.answerCallbackQuery(data.callback_query.id)
                            .finally(function () {
                                return botApi.bot.sendAttachments(data.callback_query.message.chat.id, post.attachments);
                                // .finally(function () {
                                //     var editedMessage = {
                                //             chat_id: data.callback_query.message.chat.id,
                                //             message_id: data.callback_query.message.message_id,
                                //             forceAttachments: false
                                //         };
                                //
                                //         return botApi.bot.editMessageButtons(editedMessage);
                                //     })
                                // message editing is temporary disabled
                            });
                    })
            }*/
        },
        performWebHook = function (data, response) {
            return q.Promise(function (resolve, reject) {

                response.status(200);
                response.json({status: 'OK'});

                if (!data) {
                    return reject(new Error('No webhook data specified'));
                }

                return data.message || data.inline_query || data.callback_query;
            }).then(function (user) {
                if (data.hasOwnProperty('callback_query')) {
                    var queryData = data.callback_query.data.split(' ');
                    return performCallbackQuery(queryData, data);
                } else if (data.hasOwnProperty('inline_query')) {
                    //return performInline(data.inline_query);
                    return;
                } else if (data.message) {
                    var message = data.message;

                    if (message.new_chat_member) {
                        return botApi.bot.sendMessage(message.chat.id, 'Эгегей, ёбанный в рот!');
                    } else if (message.left_chat_member) {
                        return botApi.bot.sendMessage(message.chat.id, 'Мы не будем сильно скучать.');
                    } else if (message.text) {
                        var command = (message.text || '').split(' ');
                        if (command[0].indexOf('@') >= 0) {
                            command[0] = command[0].split('@')[0];
                        }

                        if (commands[command[0]]) {
                            return performCommand(command, message, user);
                        }
                    }
                }
                console.error('unhandled message', data);
                throw new Error('No message specified');
            }).then(function (response) {
                return response;
            }).catch(function (error) {
                console.error(error);
                return error;
            });
        };

    router.get('/', function (req, res) {
        return res.send('hello from Telegram bot api');
    });

    router.get('/getMe', function (req, res, next) {
        return botApi.bot.getMe().then(function (response) {
            return res.send(JSON.stringify(response));
        }).catch(next);
    });

    router.route('/webhook')
        .post(function (req, res) {
            return performWebHook(req.body, res);
        });

    return {
        endPoint: '/bot',
        router: router
    };
};